# Revision history for sudoku

## 0.3.0 -- 2023-10-18

* update to ghc-9.4.7, lts-21.14

## 0.2.3 -- 2023-03-03

* upgrade to ghc-9.0.2, lts-19.27

## 0.2.2 -- 2021-06-04

* update tests and documentation

## 0.2.1 -- 2021-06-04

* remove redundant functions

## 0.2.0 -- 2021-06-04

* solve hard Sudoku puzzles (more efficient)

## 0.1.0 -- 2021-06-03

* solve easy Sudoku puzzles
